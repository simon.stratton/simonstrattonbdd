# simonStrattonBDD

## Heading 2

### Heading 3

**This text is in Bold**

_this text is italic_

`document.querySelector()`

```javascript
document.querySelector('.btn').click(() => {
  let input = document.querySelector('#userInput').value;
  document.querySelector('#result').innerText = input;
})


```

[This is a link to the bbc](https://www.bbc.co.uk/)


[Link to Bundle.js](../simon/bundle.js)
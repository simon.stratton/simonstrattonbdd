const { When, Then } = require('cucumber');
const { expect } = require('chai');
const calculateResult = require('../../src/calculate-result')

// This is where we store our result
// let result;

When("the user enters {int}", function(input) {
  // This is where we calculate whether it's fizz, buzz etc
  return this.result = calculateResult(input);
});

Then("the word {string} is returned", function(expectedResult) {

  // This is where we check that the result is correct
  return expect(this.result).to.equal(expectedResult);
  
});

